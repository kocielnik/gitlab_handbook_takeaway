# Reference documentation

## Rakefile

The Rakefile documents the transformations done to the manuscript of the Handbook by previous editors. It has been directly sourced from the commit by Toon Claes who submitted the gitlab.com project pull request #4519.
