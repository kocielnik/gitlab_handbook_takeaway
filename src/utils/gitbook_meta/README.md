## Introduction

The GitLab team handbook is the central repository for how we run the company.
Printed, it consists of over [7,000 pages of text](/handbook/about/#count-handbook-pages).
As part of our value of being transparent the handbook is [open to the world](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/handbook), and we welcome feedback. Please make a [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests) to suggest improvements or add clarifications.
Please use [issues](https://gitlab.com/gitlab-com/www-gitlab-com/issues) to ask questions.

