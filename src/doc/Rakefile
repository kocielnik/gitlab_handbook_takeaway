desc 'Generate ePub'
task :epub, :build do
  FileUtils.mkdir_p('tmp/handbook')
  FileUtils.mkdir_p('pdfs')
  aio_md = 'tmp/handbook/index.html.md'
  source_files = Dir['source/handbook/**/*.md']
  source_files = source_files.sort_by { |filename| File.split(filename) }
  # tune the ordering a bit (due to capital letters)
  source_files -= ['source/handbook/EE-Product-Qualification-Questions/index.md']
  source_files << 'source/handbook/EE-Product-Qualification-Questions/index.md'
  File.open(aio_md, "w+") do |aio|
    source_files.each do |filename|
      File.open(filename) do |source_file|
        contents = source_file.read

        # relative path to images so pandoc can find them
        contents.gsub!(%r{../images}, '/images')
        contents.gsub!(%r{'/images}, "'source/images")
        contents.gsub!(%r{"/images}, '"source/images')

        # prefix the anchors with the page names to avoid collissions
        anchor = Pathname.new(filename).relative_path_from(Pathname.new('source')).dirname.to_s.tr_s('/', '-')
        contents.gsub!(/^\{:(.*)#(.+)\}(.*)$/, "{:\\1##{anchor}--\\2}\\3")

        # links to the handbook should work inside the page
        contents.gsub!(%r{https?://about.gitlab.com/handbook/}, '/handbook/')

        contents.gsub!(%r{]\(/handbook/([^#\)]*)#([^\)]+)\)}) do |match|
          "](#handbook-#{$1.to_s.chomp('/').tr_s('/', '-')}--#{$2})"
        end

        contents.gsub!(%r{]\(/handbook/([^\)]+)\)}) do |match|
          if match.end_with?('png)') || match.end_with?('gif)')
            match
          else
            "](#handbook-#{$1.to_s.chomp('/').tr_s('/', '-')})"
          end
        end

        # add anchors at the top of each chapter
        unless anchor == 'handbook'
          contents.sub!(%r{---\n(?:.+:.+\n)*title: (['"]?)([^\n]+)\1\n(?:.+:.+\n)*---}, "\n# \\2\n{: ##{anchor}}\n")
        end

        aio.write(contents)
      end
    end
  end

  Dir.chdir('tmp/handbook') do
    build_cmd = %W(middleman build --source tmp/handbook)
    if !system(*build_cmd)
      raise "command failed: #{build_cmd.join(' ')}"
    end
  end
  sh 'pandoc', '-o', 'pdfs/handbook.epub', 'public/index.html'
end

