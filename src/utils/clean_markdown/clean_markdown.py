#!/usr/bin/env python3

# Usage: ./clean_markdown.py > index.md < index.html.md

def clean(line: str) -> str:

    """
    >>> clean('')
    ''
    >>> clean("<details><summary>How We Schedule Interviews on a Global Scale</summary>")
    'How We Schedule Interviews on a Global Scale'
    """

    import re

    cleanr = re.compile('<.*?>')

    cleantext = re.sub(cleanr, '', line)

    return cleantext

def process():

    from sys import stdin, exit

    try:
        for line in stdin:
            print(clean(line.replace('\n', '')))
    except KeyboardInterrupt:
        exit(1)

if __name__ == '__main__':

    process()
