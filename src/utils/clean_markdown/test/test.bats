#!/usr/bin/env bats

# Test suite for the index.html.md -> index.md conversion.

teardown() {
    rm *.out.md
}

@test "Should strip index.html.md to pure Markdown." {

  infile=../../handbook/index.html.md

  # Given
  [[ -f "$infile" ]] && [[ -f index.spec.md ]]

  # When
  ../clean "$infile" > index.out.md
  run diff index.out.md index.spec.md

  # Then
  [[ "$status" -eq 0 ]]
}

