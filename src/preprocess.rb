#!/usr/bin/env ruby

# Initial code in Ruby by Nick Thomas. Adaptation and EPUB by Toon Claes.

require 'fileutils'
require 'pathname'

# Usage example:
#
#   >> list_source_files('/nonexistent_dir')
#   => []
#
def list_source_files(path)
  source_files = Dir[path]
  source_files = source_files.sort_by { |filename| File.split(filename) }
  return source_files
end

# Usage example:
#
#   >> filter_text("")
#   => ""
#   >> filter_text("(/handbook/dashboard.png")
#   => "(handbook/dashboard.png"
#
def filter_text(contents, filename = "")
  # relative path to images so pandoc can find them
  contents.gsub!(%r{../images}, '/images')
  contents.gsub!(%r{'/images}, "'images")
  contents.gsub!(%r{"/images}, '"images')
  contents.gsub!(%r{\(/handbook}, '(handbook')

  # prefix the anchors with the page names to avoid collissions
  anchor = Pathname.new(filename).relative_path_from(Pathname.new('handbook')).dirname.to_s.tr_s('/', '-')
  contents.gsub!(/^\{:(.*)#(.+)\}(.*)$/, "{:\\1##{anchor}--\\2}\\3")

  # links to the handbook should work inside the page
  contents.gsub!(%r{https?://about.gitlab.com/handbook/}, '/handbook/')

  contents.gsub!(%r{]\(/handbook/([^#\)]*)#([^\)]+)\)}) do |match|
    "](#handbook-#{$1.to_s.chomp('/').tr_s('/', '-')}--#{$2})"
  end

  contents.gsub!(%r{]\(/handbook/([^\)]+)\)}) do |match|
    if match.end_with?('png)') || match.end_with?('gif)')
      match
    else
      "](#handbook-#{$1.to_s.chomp('/').tr_s('/', '-')})"
    end
  end

  # add anchors at the top of each chapter
  unless anchor == 'handbook'
    contents.sub!(%r{---\n(?:.+:.+\n)*title: (['"]?)([^\n]+)\1\n(?:.+:.+\n)*---}, "\n# \\2\n{: ##{anchor}}\n")
  end

  return contents
end

def get_complete_text
  source_files = list_source_files(path = 'handbook/**/*.md')

  source_files.each do |filename|
    File.open(filename) do |source_file|
      contents = source_file.read
      $stdout.print(filter_text(contents, filename))
    end
  end
end

def show_help
  puts("Usage:")
  puts("  " + $0 + " [list|clean|get_complete_text]")
  puts("Commands:")
  puts("  list: list source files in sorted order")
  puts("  clean FILE: clean text of source file")
  puts("  get_complete_text: get all text in one stream")
end

def main
  if ARGV.length > 0
    if ARGV[0] == "list"
      source_files = list_source_files(path = 'handbook/**/*.md')
      source_files.each do |filename|
        puts(filename)
      end
    elsif ARGV[0] == "get_complete_text"
      get_complete_text
    elsif ARGV[0] == "clean"
      if ARGV.length < 2
        puts("Missing argument: file")
      else
        filename = ARGV[1]
        File.open(filename) do |source_file|
          contents = source_file.read
          $stdout.print(filter_text(contents, filename))
        end
      end
    else
      show_help
    end
  else
    show_help
  end
end

if __FILE__ == $0
  main
end
