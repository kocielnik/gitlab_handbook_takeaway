# GitLab Handbook Takeaway

EPUB generator for the GitLab Handbook published by GitLab [here](https://about.gitlab.com/handbook/).

This is an answer to gitlab.com [issue 4519](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/4519).

Relevant open issue: [#85](https://gitlab.com/gitlab-org/gitlab-docs/-/issues/85).

Source files for the EPUB are available [here](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/handbook/index.html.md).

## Related issues

The case has most recently been discussed in the merge request (*MR*) [4519](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/4519). That discussion references also MR [2940](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/2940), together with issues [689](https://gitlab.com/gitlab-com/www-gitlab-com/issues/689) and [85](https://gitlab.com/gitlab-com/gitlab-docs/issues/85).

## Observations

Previous attempts to publish the EPUB version in [MR 4519](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/4519) and [issue 85](https://gitlab.com/gitlab-com/gitlab-docs/issues/85) remain at a "failing pipeline" stage and were never merged to master. This state continues even though each endeavor has been started by a person currently identifying as GitLab staff.

> I have a really hard time reading that much text on my computer as I personally enjoy having physical copies. And tbh it would be awesome to save the handbook on my bookshelf.

-- Tiago Botelho / 2016

> (...) asked by a customer, but it would be a good addition overall.

-- Achilleas Pipinellis / 2017

> I still want it...

-- Toon Claes / 2018

> Even though I'm not an employee but still I'd buy a printed copy!

-- Artem Tartakynov / 2018

No sign of currently ongoing project toward this goal has been detected on GitLab profiles of people mentioned in the most recent discussions.

## Key ideas

The key ideas are:

1. Use Pandoc on the GitLab Runner to do most of the work,
2. Isolate the generation mechanism (ie. the code) from the main repo at [gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com),
3. Share the EPUB as a release in the repository,
4. Publish release updates on a bi-monthly basis.

## The build pipeline

```bash
# On May 6th, 2019: 17.1 MB -> 23.3 MB
wget -c https://gitlab.com/gitlab-com/www-gitlab-com/-/archive/master/www-gitlab-com-master.tar.bz2?path=source%2Fhandbook -O handbook.tar.bz2

# Untar

# Lint sources

# Convert with Pandoc
```

