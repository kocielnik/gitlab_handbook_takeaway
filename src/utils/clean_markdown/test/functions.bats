#!/usr/bin/env bats

remove_tags() {
    sed "/<div/,/<\/div>/d"
}

@test "Test the tests: Should not strip tags different from div." {

  run echo "<content></content>" | remove_tags

  [[ "$output" == "<content></content>" ]]
}

@test "Should strip a pair of HTML tags." {

  run echo "<div></div>" | remove_tags

  [[ "$output" == "" ]]
}

@test "Should strip a hierarchy of HTML tags." {

  run remove_tags <<< "<div>\n  <div>\n  </div>\n </div>"

  [[ "$output" == "" ]]
}
